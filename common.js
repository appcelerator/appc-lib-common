var moment = require('alloy/moment');

var common = {

	// Add Number Separator
	addNumberSeparator: function(number) {

		if (number) {
			var wholeNumber = (number.toString().split('.'))[0];
			var decimals = (number.toString().split('.'))[1];
			var returnValue = '';
			var arr = wholeNumber.toString().split('');

			for (var i = 0; i < arr.length; i++) {
				returnValue += arr[i];
				if ((arr.length - i - 1) % 3 == 0 && i < arr.length - 1) returnValue +=
					",";
			}

			if (decimals) {
				return parseFloat(returnValue + '.' + decimals.substring(0, 2)).toFixed(2);
			}

			return returnValue + '.00';
		}

		return number;

	},

	// Build Full Address
	buildFullAddress: function(streetAddress1, streetAddress2, city, state, zip,
		country) {

		var fullAddress = common.concatenateStringWithSeparator(', ',
			streetAddress1, streetAddress2, city, state, zip, country);
		return fullAddress;

	},

	// Blur All TextFields In A View
	blurAllTextFieldsInView: function(view) {

		if (view) {
			if (view.getApiName() == 'Ti.UI.View' || view.getApiName() ==
				'Ti.UI.ScrollableView' || view.getApiName() == 'Ti.UI.ScrollView') {
				_.each(view.children, function(child) {
					if (child.getApiName() == 'Ti.UI.View') {
						common.blurAllTextFieldsInView(child);
					} else if (child.getApiName() == 'Ti.UI.TextField') {
						child.blur();
					}
				});
			}
		}

	},

	// Blur All TextFieldWidgets In A View
	blurAllTextFieldWidgetsInView: function(view) {

		if (view) {
			if (view.getApiName() == 'Ti.UI.View' || view.getApiName() ==
				'Ti.UI.ScrollableView' || view.getApiName() == 'Ti.UI.ScrollView') {
				_.each(view.children, function(child) {
					if (child.getApiName() == 'Ti.UI.View') {
						common.blurAllTextFieldsInView(child);
					} else if (child.widgetType == 'TextFieldWidget') {
						child.blurField();
					}
				});
			}
		}

	},

	// Center Map To Multiple Locations
	centerMapToMultipleLocations: function(map, latArray, longArray, zoomLevel) {

		if (latArray.length != longArray.length)
			return;
		var total_locations = latArray.length;
		var minLongi = null,
			minLati = null,
			maxLongi = null,
			maxLati = null;

		for (var i = 0; i < total_locations; i++) {
			if (minLati == null || minLati > latArray[i]) {
				minLati = latArray[i];
			}
			if (minLongi == null || minLongi > longArray[i]) {
				minLongi = longArray[i];
			}
			if (maxLati == null || maxLati < latArray[i]) {
				maxLati = latArray[i];
			}
			if (maxLongi == null || maxLongi < longArray[i]) {
				maxLongi = longArray[i];
			}
		}

		if (total_locations > 0) {
			map.setLocation({
				animate: true,
				latitude: ((maxLati + minLati) / 2),
				longitude: ((maxLongi + minLongi) / 2),
				latitudeDelta: zoomLevel,
				longitudeDelta: zoomLevel
			});
		}

	},

	// Check If Function Exists
	functionExist: function(functionToTest) {

		if (functionToTest) {
			if (typeof functionToTest === 'function') {
				return true;
			}
		}

		return false;

	},

	// Check Is Function Is Empty Or Null
	isFunctionEmpty: function(functionToTest) {

		if (functionToTest == null || functionToTest == undefined) {
			return true;
		}

		if (typeof functionToTest != 'function') {
			return true;
		}

	},

	// Check If It's Day Or Night Based On 6AM-6PM
	isDayOrNight: function() {

		return moment().isBetween(moment({
			hours: 6,
			minutes: 0,
			seconds: 0
		}), {
			hours: 18,
			minutes: 0,
			seconds: 0
		}) ? 'day' : 'night';

	},

	// Check If Object Is Empty Or Null
	isObjectEmpty: function(object) {

		if (object == null) {
			return true;
		}
		if (typeof object === 'object') {
			return Object.keys(object).length === 0;
		}

	},

	// Check If String Is Empty Or Null
	isStringEmpty: function(value) {

		return !!(value == undefined || value == null || value.toString().trim() ==
			'');

	},

	// Check Password Requirements
	checkPasswordRequirements: function(password) {

		var anUpperCase = /[A-Z]/;
		var aLowerCase = /[a-z]/;
		var aNumber = /[0-9]/;

		if (password.length < 9) {
			return false;
		}

		var numUpper = 0;
		var numLower = 0;
		var numNums = 0;
		for (var i = 0; i < password.length; i++) {
			if (anUpperCase.test(password[i]))
				numUpper++;
			else if (aLowerCase.test(password[i]))
				numLower++;
			else if (aNumber.test(password[i]))
				numNums++;
		}

		return !(numUpper < 1 || numLower < 1 || numNums < 1);

	},

	// Clean Phone Number
	cleanPhoneNumber: function(phoneNumber) {

		if (phoneNumber) {
			return phoneNumber.replace(/[^0-9]/g, '');
		}

		return phoneNumber;

	},

	// Clear View Fields
	clearViewFields: function(view) {

		if (view) {
			if (view.getApiName() == 'Ti.UI.View' || view.getApiName() ==
				'Ti.UI.ScrollableView') {
				_.each(view.children, function(child) {
					if (child.getApiName() == 'Ti.UI.View') {
						common.clearViewFields(child);
					} else if (child.getApiName() == 'Ti.UI.TextField') {
						child.setValue('');
					}
				});
			}
		}

	},

	// Concatenate String With Separator
	concatenateStringWithSeparator: function() {

		/*
		 * To use this function, the first parameter needs to be your separator
		 */

		var separator = arguments[0];
		var returnVal = '';

		for (var i = 1; i < arguments.length; i++) {
			var currentArgument = arguments[i];

			if (!common.isStringEmpty(currentArgument)) {
				returnVal += currentArgument + separator;
			}
		}

		return common.trimString(returnVal, separator);

	},

	// Convert Base64 Image To Blob
	convertBase64ImageToBlob: function(base64ImageData) {

		if (base64ImageData) {
			var mimeType = '';

			if (base64ImageData.indexOf('image/jpeg') >= 0) {
				mimeType = 'image/jpeg';
			} else if (base64ImageData.indexOf('image/png') >= 0) {
				mimeType = 'image/png';
			} else if (base64ImageData.indexOf('image/bmp') >= 0) {
				mimeType = 'image/bmp';
			} else if (base64ImageData.indexOf('image/tiff') >= 0) {
				mimeType = 'image/tiff';
			}

			return Ti.Utils.base64decode(base64ImageData.toString().replace('data:' +
				mimeType + ';base64,', '').trim());
		}

		return base64ImageData;

	},

	// Convert Latitude And Longitude To An Address
	convertLatLongToAddress: function(lat, long, callBack) {

		Ti.Geolocation.reverseGeocoder(lat, long, function(response) {
			if (response['success']) {
				var address = response['places'][0];
				callBack(address['address']);
			}
		});

	},

	// Convert Seconds To Time Format
	convertSecondsToTimeFormat: function(seconds) {

		var hrs = Math.floor(seconds / 3600);
		seconds -= hrs * 3600;
		var min = Math.floor(seconds / 60);
		seconds -= min * 60;
		var sec = parseInt(seconds % 60, 10);

		if (hrs <= 0) {
			return (min < 10 ? '0' + min : min) + ':' + (sec < 10 ? '0' + sec : sec);
		} else {
			return ((hrs < 10 ? '0' + hrs : hrs) + ':' + (min < 10 ? '0' + min : min) +
				':' + (sec < 10 ? '0' + sec : sec));
		}

	},

	// Force Remote Resource ReDownload
	forceRemoteResourceReDownload: function(resourceUrl) {

		var date = new Date();

		return resourceUrl + '?' + date.getMilliseconds();

	},

	// Format Phone Number
	formatPhoneNumber: function(phoneNumber) {

		if (phoneNumber) {
			phoneNumber = phoneNumber.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
		}

		return phoneNumber;

	},

	// Generate Random Decimal Number Between Two Numbers
	generateRandomDecimal: function(min, max, decimalPlaces) {

		var randomNumber = (Math.random() * (max - min) + min).toFixed(
			decimalPlaces);
		return randomNumber;

	},

	// Get All Text Field Widget Values
	getAllTextFieldWidgetValues: function(view) {

		var viewValues = {};

		if (view) {
			if (view.getApiName() == 'Ti.UI.View' || view.getApiName() ==
				'Ti.UI.ScrollableView' || view.getApiName() == 'Ti.UI.ScrollView') {
				_.each(view.children, function(child) {
					if (child.widgetType && child.widgetType == 'TextFieldWidget') {
						var childValue = child.getValue();

						viewValues[childValue.fieldName] = childValue.value;
					}
				});
			}
		}

		return viewValues;

	},

	// Get All Required Fields From View
	getAllRequiredFieldsFromView: function(view, requiredFields) {

		if (view) {
			if (view.getApiName() == 'Ti.UI.View' || view.getApiName() ==
				'Ti.UI.ScrollableView') {
				_.each(view.children, function(child) {
					if (child.getApiName() == 'Ti.UI.View') {
						common.getAllRequiredFieldsFromView(child, requiredFields);
					} else if (child.getApiName() == 'Ti.UI.TextField') {
						if (child.required && child.required.toLowerCase() == 'true') {
							requiredFields[child.id] = true;
						} else {
							requiredFields[child.id] = false;
						}
					}
				});
			}
		}

		return requiredFields;

	},

	// Get GPS Location
	getCurrentLocation: function(callBack) {

		common.requestGpsPermission(function(success, error) {
			if (success) {
				Ti.Geolocation.getCurrentPosition(function(position) {
					if (position) {
						var coordinates = position.coords;
						var latitude = 0;
						var longitude = 0;

						if (coordinates && coordinates.latitude) {
							latitude = coordinates.latitude;
						}
						if (coordinates && coordinates.longitude) {
							longitude = coordinates.longitude;
						}

						callBack({
							latitude: latitude,
							longitude: longitude
						});
					}
				});
			}
		});

	},

	// Get Rating Stars
	getRatingStars: function(fullStar, halfStar, emptyStar, rating, imageHeight) {

		// Get Number Of Whole Stars
		var wholeStars = Math.floor(rating);

		// Check For Half Star
		var halfStarPresent = 0;
		if (rating % 1 != 0) {
			halfStarPresent = 1;
		}

		// Get Star Positions
		var halfStarPosition = null;
		if (wholeStars < 5) {
			// Get Half Star Position
			if (wholeStars == 0 && halfStarPresent > 0) {
				halfStarPosition = 0;
			} else if (wholeStars > 0 && halfStarPresent > 0) {
				halfStarPosition = wholeStars;
			}
		}

		// Create Rating ImageView Container
		var ratingContainer = Ti.UI.createView({
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			layout: 'horizontal',
			horizontalWrap: false,
			visible: true,
			enabled: true
		});

		// Build Rating Stars
		if (wholeStars == 5) {
			// Add Full Star Images
			for (var i = 0; i < 5; i++) {
				ratingContainer.add(createImageView(fullStar, imageHeight));
			}
		} else if (halfStarPresent && halfStarPosition == 4) {
			// Add Full Star Image
			for (var i = 0; i < halfStarPosition; i++) {
				ratingContainer.add(createImageView(fullStar, imageHeight));
			}
			// Add Half Star Image
			ratingContainer.add(createImageView(halfStar, imageHeight));
		} else if (halfStarPresent && halfStarPosition < 4) {
			// Add Full Star Images
			for (var i = 0; i < halfStarPosition; i++) {
				ratingContainer.add(createImageView(fullStar, imageHeight));
			}
			// Add Half Star Image
			ratingContainer.add(createImageView(halfStar, imageHeight));
			// Add Empty Stars
			var emptyStars = (5 - (halfStarPosition)) - 1;
			for (var i = 0; i < emptyStars; i++) {
				ratingContainer.add(createImageView(emptyStar, imageHeight));
			}
		} else if (!halfStarPresent && wholeStars >= 1) {
			// Add Full Star Images
			for (var i = 0; i < wholeStars; i++) {
				ratingContainer.add(createImageView(fullStar, imageHeight));
			}
			// Add Empty Stars
			var emptyStars = (5 - wholeStars);
			for (var i = 0; i < emptyStars; i++) {
				ratingContainer.add(createImageView(emptyStar, imageHeight));
			}
		} else {
			// Add Empty Stars
			for (var i = 0; i < 5; i++) {
				ratingContainer.add(createImageView(emptyStar, imageHeight));
			}
		}

		return ratingContainer.toImage();

	},

	// Limit String
	limitString: function(string, limit) {

		if (string && string.length <= limit) {
			return string;
		}
		if (string) {
			return string.substring(0, limit) + '...';
		}
		return string;

	},

	// Make Get Call
	makeGetCall: function(url, headerArray, timeOut, successCallBack,
		errorCallBack) {

		// Create Http Client
		var client = Ti.Network.createHTTPClient({
			onload: function(response) {
				if (response.success) {
					if (successCallBack != undefined && typeof successCallBack ==
						'function') {
						successCallBack(JSON.parse(this.responseText));
					}
				} else {
					if (errorCallBack != undefined && typeof errorCallBack == 'function') {
						errorCallBack('General Failure');
					}
				}
			},
			onerror: function(response) {
				if (errorCallBack != undefined && typeof errorCallBack == 'function') {
					errorCallBack(response.error);
				}
			},
			timeout: timeOut
		});

		// Prepare Request
		client.open('GET', url);

		//Set Request Headers
		if (headerArray != undefined) {
			for (var i = 0; i < headerArray.length; i++) {
				var currentHeader = headerArray[i];

				if (currentHeader != undefined) {
					client.setRequestHeader(currentHeader['name'], currentHeader['value']);
				}
			}
		}

		// Send Request
		client.send();

	},

	// Make Post Call
	makePostCall: function(url, parameters, successCallBack, errorCallBack,
		headerArray, timeOut, useRequiredHeaders) {

		// Create Http Client
		var client = Ti.Network.createHTTPClient({
			onload: function(response) {
				if (response.success) {
					if (successCallBack != undefined && typeof successCallBack ==
						'function') {
						if (this.responseText) {
							successCallBack(JSON.parse(this.responseText));
						} else {
							successCallBack();
						}
					}
				} else {
					if (errorCallBack != undefined && typeof errorCallBack == 'function') {
						errorCallBack('General Failure');
					}
				}
			},
			onerror: function(error) {
				if (errorCallBack != undefined && typeof errorCallBack == 'function') {
					errorCallBack(error);
				}
			},
			timeout: timeOut == undefined || timeOut == null || timeOut == '' ?
				10000 : timeOut
		});

		// Prepare Request
		client.open('POST', url);

		//Set Request Headers
		if (useRequiredHeaders) {
			var requiredHeaders = Ti.App.Properties.getObject('requiredHeaders');
			if (requiredHeaders) {
				for (var i = 0; i < requiredHeaders.length; i++) {
					var currentHeader = requiredHeaders[i];

					if (currentHeader != undefined) {
						client.setRequestHeader(currentHeader['name'], currentHeader['value']);
					}
				}
			}
		} else if (headerArray != undefined) {
			for (var i = 0; i < headerArray.length; i++) {
				var currentHeader = headerArray[i];

				if (currentHeader != undefined) {
					client.setRequestHeader(currentHeader['name'], currentHeader['value']);
				}
			}
		} else {
			client.setRequestHeader('Content-Type', 'application/json');
		}

		// Send Request
		if (parameters) {
			client.send(JSON.stringify(parameters));
		} else {
			client.send();
		}

	},

	// Open Map With Driving Directions
	openMapWithDrivingDirections: function(endAddress) {

		Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
		Ti.Geolocation.activityType = Ti.Geolocation.ACTIVITYTYPE_AUTOMOTIVE_NAVIGATION;
		Ti.Geolocation.distanceFilter = 0;
		Ti.Geolocation.locationServicesAuthorization = Ti.Geolocation.AUTHORIZATION_ALWAYS;

		if (Ti.Geolocation.locationServicesEnabled) {
			Ti.Geolocation.getCurrentPosition(function(position) {
				if (position && position.coords) {
					var coordinates = position.coords;
					var latitude = coordinates.latitude;
					var longitude = coordinates.longitude;
					var destinationAddress = endAddress;
					var startAddress = null;

					if (OS_IOS) {
						common.convertLatLongToAddress(latitude, longitude, function(address) {
							if (address != null) {
								startAddress = address;
								Ti.Platform.openURL('http://maps.apple.com/?saddr=' +
									startAddress + '&daddr=' + destinationAddress);
							}
						});
					} else if (OS_ANDROID) {
						common.convertLatLongToAddress(latitude, longitude, function(address) {
							if (address != null) {
								startAddress = address;
								Ti.Platform.openURL('http://maps.google.com/maps?saddr=' +
									startAddress + '&daddr=' + destinationAddress);
							}
						});
					}
				}
			});
		} else {
			alert('Please enable location services');
		}

	},

	// Remove Phone Formatting
	removePhoneFormatting: function(phoneNumber) {

		if (phoneNumber) {
			return phoneNumber.replace(/[^\d]/g, '');
		}
		return phoneNumber;

	},

	// Remove S From End Of String And Add It To Value If The Value Is Equal Or Less Than 1
	removeSFromEnd: function(value, string) {

		if (string && value) {
			return value > 1 ? value + string : value + string.slice(0, -1);
		} else if (!value && string) {
			return 0 + string.slice(0, -1);
		} else {
			return value + string;
		}

	},

	// Request GPS Permission
	requestGpsPermission: function(callback) {

		// FIXME: Always returns false on Android 6
		// https://jira.appcelerator.org/browse/TIMOB-23135
		if (OS_IOS && !Ti.Geolocation.locationServicesEnabled) {
			return callback({
				success: false,
				error: 'Location Services Disabled'
			});
		}

		// Permissions already granted
		if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS)) {
			return callback({
				success: true
			});
		}

		// On iOS we can determine why we do not have permission
		if (OS_IOS) {

			if (Ti.Geolocation.locationServicesAuthorization === Ti.Geolocation.AUTHORIZATION_RESTRICTED) {
				return callback({
					success: false,
					error: 'Your device policy does not allow Geolocation'
				});

			} else if (Ti.Geolocation.locationServicesAuthorization === Ti.Geolocation
				.AUTHORIZATION_DENIED) {

				dialogs.confirm({
					title: 'You denied permission before',
					message: 'Tap Yes to open the Settings app to restore permissions, then try again.',
					callback: function() {
						Ti.Platform.openURL(Ti.App.iOS.applicationOpenSettingsURL);
					}
				});

				// return success:false without an error since we've informed the user already
				return callback({
					success: false
				});
			}
		}

		// Request permission
		Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS,
			function(e) {

				if (!e.success) {
					return callback({
						success: false,
						error: e.error || 'Failed to request Location Permissions'
					});
				}

				callback({
					success: true
				});
			});

	},

	// Set Interval
	setInterval: function(callback, seconds) {

		callback();
		return setInterval(callback, seconds * 1000);

	},

	// Trim String
	trimString: function(string, whatToTrim) {

		var whatToTrimLength = whatToTrim.length;
		var leftTrim = string.substr(0, whatToTrimLength);
		var rightTrim = string.substr(string.length - whatToTrimLength, string.length);

		if (leftTrim == whatToTrim) {
			string = string.substr(0, whatToTrimLength);
		}

		if (rightTrim == whatToTrim) {
			string = string.substr(0, string.length - whatToTrimLength);
		}

		return string;

	},

	// Validate Array Of Strings
	validateArrayOfStrings: function(array) {

		for (var i = 0; i < array.length; i++) {
			var currVal = array[i];

			if (common.isStringEmpty(currVal)) {
				return false;
			}
		}

		return true;

	},

	// Validate Email Address
	validateEmailAddress: function(emailAddress) {

		var re =
			/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		return re.test(emailAddress);

	},

	// Validate Object Values
	validateObjectValues: function(object) {

		if (!common.isObjectEmpty(object)) {
			for (var key in object) {
				var value = object[key];

				if (common.isStringEmpty(value)) {
					return false;
				}
			}

			return true;
		}

		return false;
	},

	// Validate View Fields - Note: This does not work with regular text fields, this is for text-field-widget only
	validateViewFields: function(view, defaultBorderColor, invalidBorderColor) {

		var viewValidated = true;

		if (view) {
			if (view.getApiName() == 'Ti.UI.View' || view.getApiName() ==
				'Ti.UI.ScrollableView' || view.getApiName() == 'Ti.UI.ScrollView') {
				_.each(view.children, function(child) {
					if (child.widgetType && child.widgetType == 'TextFieldWidget') {
						if (child.required) {
							if (!child.validate(defaultBorderColor, invalidBorderColor)) {
								viewValidated = false;
							}
						}
					}
				});
			}
		}

		return viewValidated;

	}

};

// exports
module.exports = common;

// Private Functions
function createImageView(imagePath, imageHeight) {

	var imageProperties = {
		height: imageHeight || 10,
		visible: true
	};

	if (OS_IOS) {
		imageProperties.image = imagePath;
	} else {
		imageProperties.defaultImage = imagePath;
	}

	return Ti.UI.createImageView(imageProperties);

}

// Prototype Functions
String.prototype.repeat = function(n) {

	n = n || 1;
	return new Array(n + 1).join(this);

};
